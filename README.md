## 📝 Todo List Basic App

**About the project**

This is a basic Todo List App built using React and Typescript, you can run the project locally using `npm start` or you can check the production build on:

https://todo-list-green-two.vercel.app/

Contact: vicentedepaz.contacto@gmail.com
