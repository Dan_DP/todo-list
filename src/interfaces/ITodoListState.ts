import ITodoTask from 'interfaces/ITodoTask';

interface ITodoListState {
  items: ITodoTask[];
}

export default ITodoListState;
