interface ITodoTask {
  id: number;
  title: string;
  completed: boolean;
}

export default ITodoTask;
