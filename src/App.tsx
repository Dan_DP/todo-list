import React, { useReducer } from 'react';
import todoListReducer, { initState } from 'reducers/todoList/todoListReducer';
import TodoList from 'components/TodoList/TodoList';
import {
  addTask,
  deleteTask,
  toggleTask,
} from 'reducers/todoList/todoList.actions';
import ITodoTask from 'interfaces/ITodoTask';
import './App.css';

function App() {
  const [{ items }, dispatch] = useReducer(todoListReducer, initState);
  const handleToggleTask = (id: number, completed: boolean) => {
    dispatch(toggleTask(id, completed));
  };
  const handleDeleteTask = (id: number) => {
    dispatch(deleteTask(id));
  };
  const handleAddTask = ({ completed, title }: ITodoTask) => {
    dispatch(
      addTask({
        title,
        completed,
      }),
    );
  };
  return (
    <div className="App">
      <header className="App-header">
        <p>Todo List</p>
      </header>
      <div className="list-container">
        <TodoList
          items={items}
          handleToggleTask={handleToggleTask}
          handleDeleteTask={handleDeleteTask}
          handleAddTask={handleAddTask}
        />
      </div>
    </div>
  );
}

export default App;
