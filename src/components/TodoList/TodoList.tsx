import * as React from 'react';
import { useState } from 'react';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import Checkbox from '@mui/material/Checkbox';
import IconButton from '@mui/material/IconButton';
import DeleteIcon from '@mui/icons-material/Delete';
import ITodoTask from 'interfaces/ITodoTask';
import { Button, Container, Stack, TextField } from '@mui/material';
import { Box } from '@mui/material';

interface ITodoListProps {
  items: ITodoTask[];
  handleToggleTask(id: number, completed: boolean): void;
  handleDeleteTask(id: number): void;
  handleAddTask(item: ITodoTask): void;
}

const TodoList = ({
  items,
  handleToggleTask,
  handleDeleteTask,
  handleAddTask,
}: ITodoListProps) => {
  const [newTaskTitle, setNewTaskTitle] = useState('');
  const [error, setError] = useState(false);
  const handleToggle =
    ({ id, completed }: ITodoTask) =>
    () => {
      handleToggleTask(id, !completed);
    };

  const handleDelete =
    ({ id }: ITodoTask) =>
    () => {
      handleDeleteTask(id);
    };

  const handleSubmit = (e: React.SyntheticEvent) => {
    e.preventDefault();
    setError(false);
    if (!newTaskTitle) {
      setError(true);
      return;
    }
    handleAddTask({ title: newTaskTitle, completed: false, id: 0 });
    setNewTaskTitle('');
  };

  return (
    <List sx={{ width: '100%', maxWidth: 360, bgcolor: 'background.paper' }}>
      <Container maxWidth="sm" sx={{ m: 1 }}>
        <Box
          component="form"
          sx={{
            '& > :not(style)': { m: 1, width: '25ch' },
          }}
          noValidate
          autoComplete="off"
          onSubmit={handleSubmit}
        >
          <Stack spacing={2} direction="row">
            <TextField
              required
              id="task"
              label="Task"
              variant="standard"
              helperText={error ? 'Required' : ''}
              error={error}
              value={newTaskTitle}
              onChange={({ target: { value } }) => setNewTaskTitle(value)}
            />
            <Button type="submit" variant="contained">
              Add
            </Button>
          </Stack>
        </Box>
      </Container>
      {items.map((item) => {
        const labelId = `checkbox-list-label-${item.id}`;

        return (
          <ListItem
            key={item.id}
            secondaryAction={
              <IconButton
                edge="end"
                aria-label="delete"
                onClick={handleDelete(item)}
              >
                <DeleteIcon />
              </IconButton>
            }
            disablePadding
          >
            <ListItemButton role={undefined} onClick={handleToggle(item)} dense>
              <ListItemIcon>
                <Checkbox
                  edge="start"
                  checked={item.completed}
                  tabIndex={-1}
                  disableRipple
                  inputProps={{ 'aria-labelledby': labelId }}
                />
              </ListItemIcon>
              <ListItemText id={labelId} primary={item.title} />
            </ListItemButton>
          </ListItem>
        );
      })}
    </List>
  );
};

export default TodoList;
