import ITodoTask from 'interfaces/ITodoTask';

export const enum todoListActionsTypes {
  TOGGLE_TASK = 'TOGGLE_TASK',
  DELETE_TASK = 'DELETE_TASK',
  ADD_TASK = 'ADD_TASK',
}

export interface IToggleTask {
  readonly type: todoListActionsTypes.TOGGLE_TASK;
  payload: {
    id: number;
    completed: boolean;
  };
}

export interface IDeleteTask {
  readonly type: todoListActionsTypes.DELETE_TASK;
  payload: {
    id: number;
  };
}

export interface IAddTask {
  readonly type: todoListActionsTypes.ADD_TASK;
  payload: {
    item: Omit<ITodoTask, 'id'>;
  };
}

export const toggleTask = (id: number, completed: boolean): IToggleTask => ({
  type: todoListActionsTypes.TOGGLE_TASK,
  payload: {
    id,
    completed,
  },
});

export const deleteTask = (id: number): IDeleteTask => ({
  type: todoListActionsTypes.DELETE_TASK,
  payload: {
    id,
  },
});

export const addTask = (item: Omit<ITodoTask, 'id'>): IAddTask => ({
  type: todoListActionsTypes.ADD_TASK,
  payload: {
    item,
  },
});

type TodoListActions = IToggleTask | IDeleteTask | IAddTask;

export default TodoListActions;
