import ITodoListState from 'interfaces/ITodoListState';
import TodoListActions, { todoListActionsTypes } from './todoList.actions';

export const initState: ITodoListState = {
  items: [
    {
      id: 1,
      title: 'Create React App',
      completed: true,
    },
    {
      id: 2,
      title: 'Install dependencies',
      completed: false,
    },
    {
      id: 3,
      title: 'Configure ESLint',
      completed: false,
    },
  ],
};

const todoListReducer = (
  state: ITodoListState = initState,
  action: TodoListActions,
): ITodoListState => {
  switch (action.type) {
    case todoListActionsTypes.ADD_TASK: {
      const { title } = action.payload.item;
      const { id } = state.items.reduce((prev, cur) => {
        if (prev.id < cur.id) {
          return cur;
        }
        return prev;
      });

      return {
        ...state,
        items: [
          ...state.items,
          {
            id: id + 1,
            title,
            completed: false,
          },
        ],
      };
    }

    case todoListActionsTypes.TOGGLE_TASK: {
      const newItems = state.items.map((item) => {
        if (item.id === action.payload.id) {
          return {
            ...item,
            completed: action.payload.completed,
          };
        }
        return item;
      });

      return {
        ...state,
        items: newItems,
      };
    }

    case todoListActionsTypes.DELETE_TASK: {
      const newList = state.items.filter(
        (item) => item.id !== action.payload.id,
      );

      return {
        ...state,
        items: newList,
      };
    }
  }
};

export default todoListReducer;
